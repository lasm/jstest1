/**
 * Created by dima on 3/29/17.
 */

function Base() {
    this.baseVar = 'base var';
    this.baseFunc = function() {
        console.log("base func:", this.baseVar);
        console.log("base func son var", this.sonVar);
    };
};

function mix1() {
    this.somefunt = function () {
        console.log("mix:", this.sonVar);
    };
};;

function mix2() {
    console.log("mix2:", this.sonVar);
}

function mix3() {
    console.log("mix3:", this.sonVar);
}

function SonFunc(arg) {
    Base.call(this);
    var b = Base;
    this.mmix2 = arg;
    this.sonVar = 'son var';
    mix1.call(this);
    this.sonFunc1 = function() {
        console.log('sonFunc1, sonvar:', this.sonVar);
        console.log('call baseFunc:');
        this.baseFunc();
        console.log("call b.baseFunc:");
     //   b.baseFunc();
    };
    //return {   retm: m,       retfunc: this.sonFunc1    }
};

var k = new SonFunc(mix2);
var k3 = new SonFunc(mix3);
//var kret = SonFunc();
k.sonFunc1();
//kret.retm();
k.somefunt();
//k.this = mix2;
k.mmix2();
k3.mmix2();